const Branch = function(data,leftLeaf,rightLeaf){
	const branch={};
	
	branch.data = data;
	branch.leftLeaf = leftLeaf;
	branch.rightLeaf = rightLeaf;
	branch.type="Tree";
	branch.getType = function(){
		return this.type;
	}
	branch.left = function(){
		return [this.leftLeaf.data,this.leftLeaf.leftLeaf.data,this.leftLeaf.rightLeaf.data];
	}

	branch.right = function(){
		return [this.rightLeaf.data,this.rightLeaf.leftLeaf.data,this.rightLeaf.rightLeaf.data];
	}
	branch.map = function(func){
		const arr = [];
		arr.push(func(this.data));
		arr.push(func(this.right()));
		arr.push(func(this.left()));

		return arr;
	}
	return branch;
};

const Leaf = function(){
	const leaf={};
	leaf.data="🍂";
	leaf.branch= Branch();
	leaf.getType = function(){
		return this.branch.getType();
	}
	return leaf;
};

module.exports ={
	Branch,
	Leaf
}