const { Branch, Leaf} = require("./index.js");


const leftBranch = Branch("foo", Leaf(), Leaf());
const rightBranch = Branch("bar",Leaf(),Leaf());
const tree = Branch("baz",leftBranch,rightBranch);

console.log(tree.left());
console.log(tree.map(word=>`${word}!`));
console.log(tree.getType());
console.log(Leaf().getType());