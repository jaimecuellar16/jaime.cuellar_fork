const { Branch, Leaf } = require("./index.js");


const leftBranch = new Branch("foo", new Leaf(), new Leaf());
const rightBranch = new Branch("bar", new Leaf(), new Leaf());
const tree = new Branch("baz", leftBranch, rightBranch);



console.log(tree.left); // -> ("foo", 🍂, 🍂)
console.log(tree.map(word => `${word}!`)); // -> ("baz", ("foo!", 🍂, 🍂), ("bar!", 🍂, 🍂))
console.log(tree.getType()); // -> Tree
console.log(new Leaf().getType()); // -> Tree