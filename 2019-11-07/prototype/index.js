const Branch = function(data,leftLeaf,rightLeaf){
	this.data = data;
	this.leftLeaf = leftLeaf;
	this.rightLeaf = rightLeaf;
	this.left = [this.leftLeaf.data, this.leftLeaf.leftLeaf, this.leftLeaf.rightLeaf];
	this.right = [this.rightLeaf.data,this.rightLeaf.leftLeaf,this.rightLeaf.rightLeaf];
	this.__proto__.name="Tree";
}

Branch.prototype.getType = function(){
	return Object.getPrototypeOf(this).name;
}

Branch.prototype.map = function(func){
	let arr = [];
	arr.push(func(this.data));
	arr.push(func(this.left));
	arr.push(func(this.right));

	return arr;
}
const Leaf = function(){
	this.data="🍂";
	this.__proto__.name="Tree";
}

Leaf.prototype.getType = function(){
	return Object.getPrototypeOf(this).name;
}
module.exports = {
	Branch,
	Leaf
}