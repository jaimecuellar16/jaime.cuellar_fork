class Branch{
	constructor(data, leftLeaf,rightLeaf){
		this.data = data;
		this.leftLeaf = leftLeaf;
		this.rightLeaf=rightLeaf;
		this.__proto__.name = "Tree";
	}

	left(){
		return `${[this.leftLeaf.data , this.leftLeaf.leftLeaf.data , this.leftLeaf.rightLeaf.data]}`;
	}

	right(){
		return [this.rightLeaf.data , this.rightLeaf.leftLeaf.data, this.rightLeaf.rightLeaf.data];
	}

	map(func){
		let arr = []
		arr.push(func(this.data));
		arr.push(func(this.left()));
		arr.push(func(this.right()));
		return arr;
	}

	getType(){
		return Object.getPrototypeOf(this).name;
	}
}

class Leaf{
	constructor(){
		this.data = "🍂";
		this.branch = new Branch();
	}
	getType(){
		return  this.branch.getType();
	}
}

module.exports = {
	Branch,
	Leaf
}