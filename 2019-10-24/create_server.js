const events = require("events");
const net = require("net");

const createServer = () => {
  const myEmitter = new events.EventEmitter();
  let bufferStorage = [];

  function isFinished() {
    if (
      bufferStorage.join("").substring(bufferStorage.join("").length - 4) ===
      "\r\n\r\n"
    ) {
      return true;
    } else {
      return false;
    }
  }

  function parseBuffer(buffer) {
    response = buffer.toString();
    bufferStorage.push(response);
  }

  function parsetoObject() {
    var object1;
    let cabecera;
    let metodo;
    let ruta;
    
    bufferStorage.forEach(function(element) {
      dataSplited = element.split("\r\n");
      dataSplited.forEach(data => 
        {
          if(data.includes("POST") || data.includes("GET")){
            metodo = data.split(" ")[0];
            ruta = data.split(" ")[1];
        }else if(data.includes("Accept")){
            cabecera = data.split(" ")[1];
        }
      }
      );
      
      object1 = JSON.parse(
        '{"method":"' +
          metodo +
          '", "headers":{"accept":"' +
          cabecera +
          '"}, "path":"' +
          ruta +
          '"}'
      
          );
      });
    return object1;
  }

  const parseToHttp = (code, headers, body, socket) => {
    socket.write(`HTTP/1.1 ${code}\r\n`);
    socket.write(`Content-Type: ${headers["Content-Type"]}\r\n`);
    if (headers["X-Powered-By"]) {
      socket.write(`X-Powered-By: ${headers["X-Powered-By"]}\r\n`);
    }
    socket.write("\r\n");
    socket.write(body);
    socket.write("\r\n");
    socket.end();
  };

  const server = net.createServer(socket => {
    socket.on("data", data => {
      parseBuffer(data);
      if (isFinished()) {
        const request = parsetoObject();
        bufferStorage = [];
        const response = {
          send: (code, headers, body) => {
            parseToHttp(code, headers, body, socket);
          }
        };

        myEmitter.emit("request", request, response);
      }
    });
  });

  return {
    on: (event, callback) => {
      myEmitter.on(event, callback);
    },
    listen: port => {
      server.listen(port);
    }
  };
};

module.exports = createServer;
