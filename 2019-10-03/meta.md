## Integrantes del grupo.

... ✏️
* Alvarado, David
* Cuellar, Jaime
* Cardona, Juan
* Gonzales, Luis

## Desglose de tareas
* Implementar la funcionabilidad de movimiento derecha e izquierda al "heroe"
* Hacer la puntuacion por enemigo muerto y asi mismo las vidas del heroe.

Asignado a Jaime Cuellar

* Crear el layout y estilos del juego.
* Colocar al "heroe" y colocar y hacer mover a los "villanos"
* Generar y mover los disparos de los villanos.
* Mover a los villanos hacia abajo cada cierto tiempo.

Asignado a David Alvarado

* Establecer límites de movimiento
* Impacto entre disparo y sujeto

Asignado a Juan Cardona

* Generar disparo.
* Movimiento del disparo

Asignado a Luis González