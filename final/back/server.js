const http = require("http");
const fs = require("fs");

var score = 0;

http
  .createServer(function(request, response) {
  
    if(request.method==="POST"){
      console.log(request.body);
      response.writeHead(200,{ "Content-Type":"application/json","Access-Control-Allow-Origin":"https://front-final.glitch.me"});
      const oportunidad = Math.random();
      if (oportunidad>0.5){
          response.write(JSON.stringify({replay:true,score:score}));
      }
      else{
        response.write(JSON.stringify({replay:false,score:score}));
      }
      response.end();
    }
    if (request.url === "/") {
      response.setHeader("Content-type", "text/html");
      const home = fs.readFileSync("./game.html");
      response.end(home);
    }
    fs.readFile("./" + request.url, function(err, data) {
      if (!err) {
        var dotoffset = request.url.lastIndexOf(".");
        var mimetype =
          dotoffset == -1
            ? "text/plain"
            : {
                ".html": "text/html",
                ".ico": "image/x-icon",
                ".jpg": "image/jpeg",
                ".png": "image/png",
                ".gif": "image/gif",
                ".css": "text/css",
                ".js": "text/javascript"
              }[request.url.substr(dotoffset)];
        response.setHeader("Content-type", mimetype);
        response.end(data);
        console.log(request.url, mimetype);
      } else {
        console.log("file not found: " + request.url);
        const home = fs.readFileSync("./game.html");
        response.end(home);
      }
    });
  })
  .listen(8080, "localhost");