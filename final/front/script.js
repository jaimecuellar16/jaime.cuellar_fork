//const { Stream } = window;
const { redux } = window;

const gamePad = document.querySelector(".game-pad");
var heroeLife = 100;
var score = 0;
window.onload = () => {
  setInterval(() => {
    generateVillians();
    moveVillanos();
    villiansShoot();
  }, 2000);
  setInterval(() => {
    moveMisil();
  }, 200);
};

const DISTANCE = 10;
var LEFT = false;
var RIGHT = false;
var UP = false;
var DOWN = false;
const misiles = [];
const villanos = [];
const misilesVillanos=[];
function heroeHitted(state={hitted:false},action){
  switch(action.hitted){
    case "HIT":
      return {...state,hitted:true};
    default:
      return state;
  }
}

function updateDirection(
  state = {
    isLeft: false,
    isRight: false,
    isUp: false,
    isDown: false,
    isShot: false
  },
  action
) {
  switch (action.direction) {
    case "RIGHT":
      return {
        ...state,
        isLeft: false,
        isRight: true,
        isUp: false,
        isDown: false,
        isShot: false
      };
    case "LEFT":
      return {
        ...state,
        isLeft: true,
        isRight: false,
        isUp: false,
        isDown: false,
        isShot: false
      };
    case "UP":
      return {
        ...state,
        isLeft: false,
        isRight: false,
        isUp: true,
        isDown: false,
        isShot: false
      };
    case "DOWN":
      return {
        ...state,
        isLeft: false,
        isRight: false,
        isUp: false,
        isDown: true,
        isShot: false
      };
    case "SHOT":
      return {
        ...state,
        isLeft: false,
        isRight: false,
        isUp: false,
        isDown: true,
        isShot: true
      };
    default:
      return state;
  }
}

function heroeDied(state={died:false},action){
  switch(action.died){
    case "DIED":
      return { ...state, died:true};
    default:
      return state;
  }
}

const heroeDiedStore = redux.createStore(heroeDied);
heroeDiedStore.subscribe(()=>{
  const state = heroeDiedStore.getState();
  if(state.die){
    console.log("MURIO");
    //Abrir modal y si da ok mandar solicitud al server
  }
})

const heroeHittedStore = redux.createStore(heroeHitted);
heroeHittedStore.subscribe(()=>{
  const states = heroeHittedStore.getState();
  
  if(states.hitted){
    heroeLife = heroeLife - 10;
    console.log(heroeLife);
    var life = document.getElementById("progress-bar-js").value;
    document.getElementById("progress-bar-js").value -=10;
    if (heroeLife==0){
      heroeDiedStore.dispatch({die:"DIED"}); 
       console.log("hi");
      const replay = confirm("Quieres probar tu suerte y recibir otra oportunidad?");
      if(replay){
        var data = {
        score:score,
        replay:true
      };
      fetch("https://back-final.glitch.me/score",{method:"POST" ,body: JSON.stringify(data)})
        .then(res =>{res.json().then(data =>{
           if(data.replay===true){
             heroeLife = 100;
             document.getElementById("progress-bar-js").value=100;
           }
          else{
            alert("Mas suerte en la proxima");
            location.reload();
          }
          });
        })
        .catch(err => console.log(err));
      }
      else{
        location.reload();
      }
    }
  }
});

const store = redux.createStore(updateDirection);
const moveMisil = () => {
  misiles.forEach(misil => {
    const coords = misil.elem.getBoundingClientRect();
    if (misil.direccion === "right") {
      if (coords.right + DISTANCE < 1240) {
        misil.elem.style.left = `${coords.left + DISTANCE}px`;
      } else {
        if (misil.elem.parentNode) {
          misil.elem.parentNode.removeChild(misil.elem);
        }
      }
    }
    if (misil.direccion === "left") {
      if (coords.left > 30) {
        misil.elem.style.left = `${coords.left - DISTANCE}px`;
      } else {
        if (misil.elem.parentNode) {
          misil.elem.parentNode.removeChild(misil.elem);
        }
      }
    }
    if (misil.direccion === "up") {
      if (coords.top - DISTANCE > 25) {
        misil.elem.style.top = `${coords.top - DISTANCE}px`;
      } else {
        if (misil.elem.parentNode) {
          misil.elem.parentNode.removeChild(misil.elem);
        }
      }
    }
  });
  misilesVillanos.forEach(misil => {
    const coords = misil.elem.getBoundingClientRect();
    if (misil.direccion === "right") {
      if (coords.right + DISTANCE < 1240) {
        misil.elem.style.left = `${coords.left + DISTANCE}px`;
      } else {
        if (misil.elem.parentNode) {
          misil.elem.parentNode.removeChild(misil.elem);
        }
      }
    }
    if (misil.direccion === "left") {
      if (coords.left > 30) {
        misil.elem.style.left = `${coords.left - DISTANCE}px`;
      } else {
        if (misil.elem.parentNode) {
          misil.elem.parentNode.removeChild(misil.elem);
        }
      }
    }
    if (misil.direccion === "up") {
      if (coords.top - DISTANCE > 25) {
        misil.elem.style.top = `${coords.top - DISTANCE}px`;
      } else {
        if (misil.elem.parentNode) {
          misil.elem.parentNode.removeChild(misil.elem);
        }
      }
    }
  });
  
  misiles.forEach(misil=>{
    const misilCoords = misil.elem.getBoundingClientRect();
    const impacted = impact(misil.elem,"villano");
    if(impacted) {
      impacted.parentElement.removeChild(impacted);
      score = score + 10;
      document.querySelector(".score-js").innerHTML = score;
    }
    
  })
  misilesVillanos.forEach(misil=>{
    const misilCoords = misil.elem.getBoundingClientRect();
    //const impacted = impact(misil.elem,"villano");
    const heroeImpacted = impact(misil.elem, "heroe");
    if(heroeImpacted){
      heroeHittedStore.dispatch({hitted:"HIT"});
    }
  })
};

const generateVillians = () => {
  const villano = document.createElement("div");
  const villanoImg = document.createElement("img");
  villano.style.zIndex = 1;
  villano.style.position = "absolute";
  villano.style.height = "50px";
  villano.style.width = "20px";
  villano.className = "villano";
  villanoImg.setAttribute(
    "src",
    "https://www.pngkit.com/png/full/237-2372726_metal-slug-pixel-png-metal-slug-soldier-png.png"
  );
  villanoImg.setAttribute("height", "100%");
  villanoImg.setAttribute("width", "100%");
  villano.appendChild(villanoImg);
  const ranVillian = Math.random();
  if (ranVillian < 1 && ranVillian > 0.6) {
    const salidaX = Math.random();
    if (salidaX > 0.5) {
      villano.style.left = `${30}px`;
    } else {
      villano.style.left = `${1215}px`;
    }
    const salidaY = Math.random() * (490-40) + 40;
    villano.style.top = `${salidaY}px`;
    gamePad.appendChild(villano);
    if (villano.style.left === "30px") {
      villanos.push({ salida: 0, elem: villano });
    } else {
      villanos.push({ salida: 1, elem: villano });
    }
  }
};

const impact = (elem, hitted) =>{
  const {x,y} = elem.getBoundingClientRect();
  const elems = document.elementsFromPoint(x,y);
  var impacted = null;
  
  elems.forEach(element=>{
    if(element.className===hitted){
      impacted=element;
      elem.parentElement.removeChild(elem);
    }
  });
  
  return impacted;
}

const moveVillanos = () => {
  villanos.forEach(villano => {
    const villCoords = villano.elem.getBoundingClientRect();
    if (villano.salida === 0) {
      if(villCoords.right + DISTANCE < 1240){
        villano.elem.style.left = `${villCoords.left + DISTANCE}px`;
      }
      else{
        villano.elem.style.left = `${villCoords.left - DISTANCE}px`;
      }
    } else {
      if(villCoords.left > 30){
          villano.elem.style.left = `${villCoords.left - DISTANCE}px`;
      }
      else{
          villano.elem.style.left = `${villCoords.left + DISTANCE}px`;
      }
    }
  });
};

function updateShots(
  state = {
    isFired: false
  },
  action
) {
  switch (action.fire) {
    case "SHOOT":
      return { ...state, isFired: true };
    default:
      return state;
  }
}
const villiansStore = redux.createStore(updateShots);

villiansStore.subscribe(() => {
  const states = villiansStore.getState();
  if (states.isFired) {
    const misilVillano = document.createElement("div");
    const misilVillanoImg = document.createElement("img");
    misilVillano.style.height = "25px";
    misilVillano.style.width = "25px";
    misilVillano.style.position = "absolute";
    misilVillano.style.borderRadius = "50%";
    const randVillian = Math.floor(Math.random() * villanos.length);
    const villano = villanos[randVillian];
    const villCoords = villano.elem.getBoundingClientRect();
    const direc = villano.salida;
    if (direc === 0) {
      const heroe = document.querySelector(".heroe");
      const heroeCoords = heroe.getBoundingClientRect();
      misilVillanoImg.setAttribute(
      "src",
      "https://i.pinimg.com/originals/64/b3/7c/64b37cb887ccaf2ced92c20e60a3aed5.png"
      )
      misilVillanoImg.setAttribute("height", "100%");
      misilVillanoImg.setAttribute("width", "100%");
      misilVillano.style.left = `${villCoords.left + 10}px`;
      misilVillano.style.top = `${villCoords.top + 5}px`;
      misilVillano.appendChild(misilVillanoImg);
      misilesVillanos.push({ direccion: "right", elem: misilVillano });
      gamePad.appendChild(misilVillano);
      const misilVillanoCoords = misilVillano.getBoundingClientRect();
      console.log(misilVillanoCoords);
      console.log(heroeCoords);
      const pendiente =
        (heroeCoords.y - misilVillanoCoords.y) /
        (heroeCoords.x - misilVillanoCoords.x);
      console.log(pendiente);
    }
    if (direc === 1) {
      const heroe = document.querySelector(".heroe");
      const heroeCoords = heroe.getBoundingClientRect();
      misilVillano.style.left = `${villCoords.left - 10}px`;
      misilVillano.style.top = `${villCoords.top + 5}px`;
      misilVillanoImg.setAttribute(
      "src",
      "https://i.pinimg.com/originals/64/b3/7c/64b37cb887ccaf2ced92c20e60a3aed5.png"
      )
      misilVillanoImg.setAttribute("height", "100%");
      misilVillanoImg.setAttribute("width", "100%");
      misilVillano.appendChild(misilVillanoImg);
      misilesVillanos.push({ direccion: "left", elem: misilVillano });
      gamePad.appendChild(misilVillano);
      const misilVillanoCoords = misilVillano.getBoundingClientRect();
      const pendiente =
        (heroeCoords.y - misilVillanoCoords.y) /
        (heroeCoords.x - misilVillanoCoords.x);
      console.log(pendiente);
    }
  }
});

const villiansShoot = () => {
  const rand = Math.random();
  if (rand >0.1 && rand <0.8) {
    villiansStore.dispatch({ fire: "SHOOT" });
  }
};

store.subscribe(() => {
  const states = store.getState();
  const misil = document.createElement("div");
  const misilImg = document.createElement("img");
  const heroe = document.querySelector(".heroe");
  const coordenadas = heroe.getBoundingClientRect();
  misil.style.zIndex = 1;
  misil.style.position = "absolute";
  misil.classList.add("misil");

  if (states.isLeft) {
    if (coordenadas.left > 30) {
      heroe.style.left = `${coordenadas.left - DISTANCE}px`;
    }
  }
  if (states.isRight) {
    if (coordenadas.right + DISTANCE < 1240) {
      heroe.style.left = `${coordenadas.left + DISTANCE}px`;
    }
  }
  if (states.isUp) {
    if (coordenadas.top - DISTANCE > 40) {
      heroe.style.top = `${coordenadas.top - DISTANCE}px`;
    }
  }
  if (states.isDown) {
    if (coordenadas.top + DISTANCE < 500) {
      heroe.style.top = `${coordenadas.top + DISTANCE}px`;
    }
  }
  if (states.isShot) {
    if (LEFT) {
      misilImg.setAttribute(
      "src",
      "https://cdn.pixabay.com/photo/2013/07/12/16/29/missile-150987_960_720.png"
      )
      misil.style.height = "20px";
      misil.style.width = "30px";
      misil.style.left = `${coordenadas.left - 12}px`;
      misil.style.top = `${coordenadas.top + 20}px`;
      misilImg.setAttribute("height", "100%");
      misilImg.setAttribute("width", "100%");
      misil.appendChild(misilImg);
      gamePad.appendChild(misil);
      misiles.push({ direccion: "left", elem: misil });
    }
    if (RIGHT) {
      misilImg.setAttribute(
      "src",
      "https://pngimage.net/wp-content/uploads/2018/06/misil-png-2.png"
      )
      misil.style.height = "20px";
      misil.style.width = "30px";
      misil.style.left = `${coordenadas.left + 30}px`;
      misil.style.top = `${coordenadas.top + 20}px`;
      misilImg.setAttribute("height", "100%");
      misilImg.setAttribute("width", "100%");
      misil.appendChild(misilImg);
      gamePad.appendChild(misil);
      misiles.push({ direccion: "right", elem: misil });
    }
    if (UP) {
      misilImg.setAttribute(
      "src",
      "https://cdn.pixabay.com/photo/2013/07/12/12/41/rocket-146104_960_720.png"
      )
      misilImg.setAttribute("height", "100%");
      misilImg.setAttribute("width", "100%");
      misil.style.height = "40px";
      misil.style.width = "20px";
      misil.style.left = `${coordenadas.left + 5}px`;
      misil.style.top = `${coordenadas.top + 1}px`;
      misil.appendChild(misilImg);
      gamePad.appendChild(misil);
      misiles.push({ direccion: "up", elem: misil });
    }
  }
});

document.addEventListener("keydown", e => {
  if (e.code === "ArrowLeft") {
    store.dispatch({ direction: "LEFT" });
    LEFT = true;
    RIGHT = false;
    UP = false;
    DOWN = false;
  }
  if (e.code === "ArrowRight") {
    store.dispatch({ direction: "RIGHT" });
    LEFT = false;
    RIGHT = true;
    UP = false;
    DOWN = false;
  }
  if (e.code === "ArrowUp") {
    store.dispatch({ direction: "UP" });
    LEFT = false;
    RIGHT = false;
    UP = true;
    DOWN = false;
  }
  if (e.code === "ArrowDown") {
    store.dispatch({ direction: "DOWN" });
    LEFT = false;
    RIGHT = false;
    UP = false;
    DOWN = true;
  }
  if (e.code === "Space") {
    store.dispatch({ direction: "SHOT" });
  }
});



