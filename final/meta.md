Nombre del juego: "Imitacion (barata) Metal Slug".

URL del servidor desplegado: `https://back-final.glitch.me/`.


### Implementacion
Es un tipo de juego inspirado en el metal slug, donde consiste elimnar a tus enemigos que aparecen de forma aleatoria en el mapa, al morir se hace una interaccion con el servidor, preguntando al usuario si desea tirar los dados y recibir otra oportunidad de continuar o no.

### Jugabilidad
Mueves al heroe utilizando las teclas de flechas, y dependiendo de el ultimo movimiento hacia esa direccion saldra el disparo.